import logging
import re


# 处理wolai路径
def trim_wolai_ends(str):
    match = re.search(r'_\w{20,22}$', str)

    if match is not None:
        str = str[:match.start()]

    return str



def replace_note_link(note_path, older, newer):
    try:
        with open(note_path, 'rt', encoding='UTF-8') as f:
            content = f.read()

        content = content.replace(older, newer)
        with open(note_path, 'wt', encoding='UTF-8') as f:
            f.write(content)

    except Exception as e:
        logging.error(e)