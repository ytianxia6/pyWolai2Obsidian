from pathlib import Path
from .note import Note
from .file import File
import logging


class Wolai(object):
    def __init__(self, folder):
        self._root_folder = folder
        self._root_notes = []
        self._notes = {}

        self._lostfiles = []

        self.read()

    @property
    def lostfiles(self):
        return self._lostfiles

    @property
    def root_notes(self):
        return self._root_notes

    # 读根目录md文件
    def read_root_notes(self):
        logging.info('read root note in ' + str(self._root_folder))
        folder = Path(self._root_folder)
        if not folder.is_dir():
            raise Exception("未指定根目录")

        # 找到根目录中的md文件
        for x in folder.iterdir():
            if x.is_file() and x.suffix.lower() == ".md":
                logging.info('找到根日记: ' + str(x))
                self._root_notes.append(self.append_wfile(x, None))

    # 对每个Md文件，
    def read_note(self, note):
        notes = []

        # 链接
        for link in note.links:

            p = Path(note.folder).joinpath(link).resolve()
            if not p.exists():
                continue

            subnote = self.append_wfile(p, note)
            subnote.set_origin(link)
            if isinstance(subnote, Note):
                notes.append(subnote)

        for subnote in notes:
            if isinstance(subnote, Note):
                self.read_note(subnote)

    # 读目录
    def read(self):
        # 读根目录
        self.read_root_notes()

        # 遍历所有note的链接，加入进来
        for note in self._root_notes:
            self.read_note(note)

        # 找到漏掉的文件
        self.search_lost_files()

    def append_wfile(self, note_path, parent_note):
        logging.info('读取文件 ' + str(note_path))
        wfile = self.create_wfile(note_path)
        if parent_note is None:
            return wfile

        parent_note._linkfiles.append(wfile)
        if isinstance(wfile, Note):
            wfile._isLinked.append(parent_note)

        return wfile

    def create_wfile(self, note_path):
        a = str(note_path.absolute())
        if a in self._notes.keys():
            return self._notes[a]

        relative_dir = note_path.parent.relative_to(self._root_folder)
        fname = note_path.stem
        ext = note_path.suffix
        isNote = ext.lower() == ".md"
        try:
            cls = Note if isNote else File
            note = cls(self._root_folder, relative_dir, fname, ext)

            self._notes[a] = note

            return note
        except:
            print("error file : " + str(note_path))
            return None

    def search_lost_files(self):
        logging.info('开始检查丢失文件')
        d = Path(self._root_folder)
        for x in d.glob('**/*'):
            if x.is_dir():
                continue

            if str(x.absolute()) in self._notes.keys():
                continue

            logging.warning('找到未处理文件: ' + str(x))
            wfile = self.append_wfile(x, None)
            self._lostfiles.append(wfile)

