import markdown
from lxml import etree
from .file import File
from pathlib import Path
import re
import logging


class Note(File):
    def __init__(self, root, parent, filename, ext):
        super(Note, self).__init__(root, parent, filename, ext)
        self._links = []
        self._linkfiles = []
        self._isLinked = []
        # self._attatchments = []

        self.parse_links()
        self.parse_attachments()

    def parse_links(self):
        self.parse_url('href')

    def parse_attachments(self):
        self.parse_url('src')

    def parse_url(self, tag):
        logging.info('parse link with ' + tag + ' for ' + self.path)
        with open(self.path, 'r', encoding='UTF-8') as f:
            content = f.read()

            html = markdown.markdown(content)
            links = list(set(re.findall(tag + r'=["]?([^">]+)', html)))
            links = list(filter(lambda l: l[0] != "{", links))

            self._links += links

    @property
    def links(self):
        return self._links

    @property
    def has_child_page(self):
        return any(isinstance(l, Note) for l in self._linkfiles)
