import os.path


class File(object):
    def __init__(self, root, parent, filename, ext):
        self._root = root
        self._parent = parent
        self._filename = filename
        self._ext = ext

        self._origin = None

    @property
    def path(self):
        return os.path.join(self.folder, self.filename)

    @property
    def folder(self):
        return os.path.join(self._root, self._parent)

    @property
    def filepath_without_ext(self):
        return os.path.join(self.folder, self._filename)

    @property
    def filename_without_ext(self):
        return self._filename

    @property
    def filename(self):
        return self._filename + self._ext

    def set_origin(self, str):
        self._origin = str

    @property
    def origin(self):
        return self._origin
