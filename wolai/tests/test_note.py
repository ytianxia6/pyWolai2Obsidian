import os
from pathlib import Path
from unittest import TestCase

from wolai.note import Note


class TestNote(TestCase):
    def test_parse_markdown(self):
        dir = Path.cwd()
        note = Note(dir.parent.parent.absolute(), "test", "笔记本", ".md")

    def test_read_20221107(self):
        dir = Path.cwd().parent.parent.joinpath('test')
        note = Note(dir, 'pages', '产品_vEcAhfLLedb9WTGfKCKmM', ".md")

        self.assertEqual(len(note.links), 1)
        self.assertEqual(note.links[0], "2022-11-07 机械紧急问题支持_eKeszFvZJb6YGP7N84ebpR.md")

    def test_read_手写笔记(self):
        dir = Path.cwd().parent.parent.joinpath('test')
        note = Note(dir, 'pages', '手写笔记_8juQAjwGatHGbM2TaToXPB', ".md")

        self.assertEqual(len(note.links), 1)
        self.assertEqual(note.links[0], "../file/6c88b9838ab404b444de10bcc3994913_pQOZWdurx7.null")

    def test_read_配置v80(self):
        dir = Path.cwd().parent.parent.joinpath('test')
        note = Note(dir, 'pages', '配置 v80 编译选项_oUr8s9KFSjmUJVFhbBNFte', ".md")

        self.assertEqual(len(note.links), 3)
#        self.assertEqual(note.attatchments[0], "../image/de7041d560295de60b705b841a6895c8_S_I20zVZfw.png")

    def test_read_翻译知识(self):
        dir = Path.cwd().parent.parent.joinpath('test')
        note = Note(dir, 'pages', '翻译知识_7ZGLBMbsZpdp3WfdDjLVkR', ".md")

        self.assertTrue(note.links.__contains__('The Complete Idiot\'s Guide to Writing Namespace Ex_rNoszPB8Wxr7yNQgGH5Cdy.md'))
#        self.assertEqual(len(note.attatchments), 3)
#        self.assertEqual(note.attatchments[0], "../image/de7041d560295de60b705b841a6895c8_S_I20zVZfw.png")

    def test_read_网络配置(self):
        dir = Path.cwd().parent.parent.joinpath('test')
        note = Note(dir, 'pages', '网络配置_w3D5j2kpz2szNyy1FiesRu', ".md")

        self.assertEqual(1, len(note.links))
        self.assertTrue(note.links.__contains__('../image/image_KO8FZ6tWwf.png'))
