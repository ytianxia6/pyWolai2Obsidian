from unittest import TestCase
from pathlib import Path
from wolai.wolai import Wolai

class TestWolai(TestCase):
    def test_read(self):
        dir = Path.cwd()
        wolai = Wolai(dir.parent.parent.joinpath("test"))

        self.assertEqual(0, len(wolai.lostfiles))

