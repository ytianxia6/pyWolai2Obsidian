import logging
import re
from pathlib import Path
import shutil

import config
from utils import trim_wolai_ends, replace_note_link
from wolai import Note


class WolaiExporter(object):
    def __init__(self, wolai, target):
        self._wolai = wolai
        self._target = target

    def export(self):
        # 确保目标文件夹存在
        target = Path(self._target)
        if target.exists():
            if not target.is_dir():
                raise Exception('目标必须是已经存在的文件夹')

            shutil.rmtree(target, ignore_errors=True)

        target.mkdir()

        for note in self._wolai.root_notes:
            self.export_note(note, target)

    def export_note(self, note, target):
        if not isinstance(note, Note):
            target_folder = target
            target_folder = target_folder.joinpath(config.attachemtns)

            return self.single_export_note(note, target_folder)
        elif not note.has_child_page:   # 没有子页面

            # 终端文档
            new_note = self.single_export_note(note, target)
            for link in note._linkfiles:
                new_path = self.export_note(link, target)
                new_path = new_path.relative_to(target)
                self.replace_link(new_note, link.origin, str(new_path))

            return new_note
        else:
            subdir = target.joinpath(trim_wolai_ends(note.filename_without_ext))
            new_note = self.single_export_note(note, subdir)

            # 链接
            for link in note._linkfiles:
                new_path = self.export_note(link, subdir)
                new_path = new_path.relative_to(subdir)
                self.replace_link(new_note, link.origin, str(new_path))

            return new_note

    def single_export_note(self, note: Note, target: Path):
        return self.single_move_file(note.path, target)

        # att_dir = target.joinpath('.attachments')
        # for att in note.attatchments:
        #     self.single_move_file(att, att_dir)

    def single_move_file(self, file, target):
        if not target.exists():
            target.mkdir()

        # 由于源文件都是使用 '_' + 22位字符文件名，需要将文件名重定义
        source = Path(file)

        dest = target.joinpath(source.name)
        dest = dest.with_stem(trim_wolai_ends(source.stem))


        shutil.copy(file, str(dest))
        return dest

    def replace_link(self, note_path, older, newer):
        replace_note_link(note_path, older, newer)
