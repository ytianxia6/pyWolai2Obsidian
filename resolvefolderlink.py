import re
from pathlib import Path

import utils
from wolai import Note


# 处理文件夹链接
# 对于文件夹链接形式， 即 'XX/XX.md',校正为 'XX'
def resolve_folder_link(src: str):
    root = Path(src).resolve()
    for x in root.glob('**/*'):
        if not x.is_file() or x.suffix.lower() != '.md':
            continue
        print('扫描文件 ' + str(x))
        note = Note(root, x.parent.relative_to(root), x.stem, x.suffix)
        for l in note.links:
            match = re.match(r'(.+)\\\1\.md', l)
            if match:
                new_link = match.group(1)
                print('替换文件 {} 中的链接 {} 为 {}'.format(x, l, new_link))
                utils.replace_note_link(x, l, new_link)
