# 这是一个示例 Python 脚本。

# 按 Shift+F10 执行或将其替换为您的代码。
# 按 双击 Shift 在所有地方搜索类、文件、工具窗口、操作和设置。
import click
from wolai import Wolai
from wolaiexporter import WolaiExporter
from resolvefolderlink import resolve_folder_link
import logging

@click.group()
def cli():
    pass

@click.command()
@click.argument('folder')
def check_wolai(folder):
    try:
        logging.info('准备检查日记目录：' + folder)
        wolai = Wolai(folder)
        if len(wolai.lostfiles) > 0:
            logging.error("\n检查到错误文件：")
            for f in wolai.lostfiles:
                logging.error("\n" + str(f))

            return -1
        else:
            logging.info('导出' + len(wolai._notes.keys()) + '个文件')
            logging.info('目录正常，可以导出')
            return 0
    except:
        logging.error("\n异常退出")
        return -2

@click.command()
@click.argument('src')
@click.argument('dest')
def export_wolai(src, dest):
    try:
        wolai = Wolai(src)

        exporter = WolaiExporter(wolai, dest)
        exporter.export()

        print('导出成功')
    except Exception as ex:
        logging.error(ex)

@click.command()
@click.argument('src')
def resolve_folder_link(src):
    resolve_folder_link(src)

# 按装订区域中的绿色按钮以运行脚本。
if __name__ == '__main__':
    cli()

