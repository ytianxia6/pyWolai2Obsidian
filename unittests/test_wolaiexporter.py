import os
from unittest import TestCase
from wolai import Wolai
from pathlib import Path

from wolaiexporter import WolaiExporter


class TestWolaiExporter(TestCase):
    def test_export(self):
        try:
            dir = Path.cwd()
            os.chdir(str(dir.parent))
            wolai = Wolai(dir.parent.joinpath("test"))

            exporter = WolaiExporter(wolai, dir.parent.joinpath('test2'))
            exporter.export()
        except:
            self.fail('异常退出')

