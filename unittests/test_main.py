from unittest import TestCase
from click.testing import CliRunner
from main import check_wolai, export_wolai
from pathlib import Path

class Test(TestCase):
    def test_check_wolai(self):
        dir = Path.cwd().parent

        runner = CliRunner()
        result = runner.invoke(check_wolai, [str(dir.joinpath('test'))])

        assert result.exit_code == 0

    def test_export_wolai(self):
        dir = Path.cwd().parent

        runner = CliRunner()
        result = runner.invoke(export_wolai, [str(dir.joinpath('test')), str(dir.joinpath('test2'))])
