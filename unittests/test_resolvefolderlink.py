import os
import re
from unittest import TestCase

from pathlib import Path
from resolvefolderlink import resolve_folder_link


class TestResolveFolderLink(TestCase):
    def test_export(self):
        try:
            dir = Path.cwd().parent.joinpath('test_link')
            resolve_folder_link(str(dir))
        except:
            self.fail('异常退出')

    def test_regex(self):
        str = r'nodejs\nodejs.md'
        pattern = r'(.+)\\\1\.md'
        match = re.search(pattern, str)
        self.assertIsNotNone(match)
