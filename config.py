import configparser
import logging
from pathlib import Path

# 初始化
cp = configparser.ConfigParser()

filename = 'config.ini'

# 读取文件
file = Path(filename).resolve()
if not file.exists():
    file = Path.cwd().joinpath(filename)

if not file.exists():
    file = Path.cwd().parent.joinpath(filename)

if not file.exists():
    logging.error('config file {} not exist!'.format(str(file)))
    raise Exception('config file not found!')

cp.read(str(file), encoding='utf-8')

attachemtns = cp.get('config', 'attachments')
